export interface ServersState {
    loading: boolean;
    error: null | string;
    servers: any[];
}

export interface Server {
    name: string;
    distance: string;
}

export enum ServersActionTypes {
    GET_SERVERS = 'GET_SERVERS',
    GET_SERVERS_SUCCESS = 'GET_SERVERS_SUCCESS',
    GET_SERVERS_ERROR = 'GET_SERVERS_ERROR'
}

export interface GetServersAction {
    type: ServersActionTypes.GET_SERVERS;
}

export interface GetServersSuccessAction {
    type: ServersActionTypes.GET_SERVERS_SUCCESS;
    payload: any[];
}

export interface GetServersErrorAction {
    type: ServersActionTypes.GET_SERVERS_ERROR;
    payload: string;
}

export type ServersActions = GetServersAction | GetServersSuccessAction | GetServersErrorAction;
