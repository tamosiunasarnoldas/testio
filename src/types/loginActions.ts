export interface LoginState {
    loading: boolean;
    error: null | string;
    token?: string;
}

export enum LoginActionTypes {
    LOGIN = 'LOGIN',
    LOGIN_SUCCESS = 'LOGIN_SUCCESS',
    LOGIN_ERROR = 'LOGIN_ERROR'
}

export interface LoginAction {
    type: LoginActionTypes.LOGIN;
}

export interface LoginSuccessAction {
    type: LoginActionTypes.LOGIN_SUCCESS;
    payload: string;
}

export interface LoginErrorAction {
    type: LoginActionTypes.LOGIN_ERROR;
    payload: string;
}

export type LoginActions = LoginAction | LoginSuccessAction | LoginErrorAction;
