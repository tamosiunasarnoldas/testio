import { LoginActions, LoginActionTypes, LoginState } from "../../types/loginActions";

const initialState: LoginState = {
    loading: false,
    error: null,
    token: undefined
}

export const loginReducer = (state = initialState, action: LoginActions): LoginState => {
    switch (action.type) {
        case LoginActionTypes.LOGIN:
            return {
                ...initialState, loading: true
            }
        case LoginActionTypes.LOGIN_SUCCESS:
            return {
                ...initialState, loading: false, token: action.payload
            }
        case LoginActionTypes.LOGIN_ERROR:
            return {
                ...initialState, loading: false, error: action.payload
            }
        default:
            return state
    }
}
