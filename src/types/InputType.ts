export interface InputType {
    placeholder: string;
    imageSource: string;
    name: string;
    type?: 'text' | 'password';
}
