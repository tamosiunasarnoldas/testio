import React, { FormEvent } from 'react';
import styled from "styled-components";
import Input from "./Input";
import User from "../assets/User.svg";
import Lock from "../assets/Lock.svg";
import Button from "./Button";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { useActions } from "../hooks/useActions";
import { useHistory } from "react-router-dom";
import { store } from "../store";
import { LoginActionTypes } from "../types/loginActions";
import { Pages } from "../types/Paths";

const Form = styled.form`
  display: flex;
  flex-direction: column;
  gap: 1.5rem;
  width: 100%;
  max-width: 361px;
`
const formFields = {
    username: 'username',
    password: 'password'
};

const LoginForm = () => {
    const {loading} = useTypedSelector(state => state.login);
    const {login} = useActions();
    const history = useHistory();

    const OnFormSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formData = new FormData(e.target as HTMLFormElement);
        const username = formData.get(formFields.username) as string;
        const password = formData.get(formFields.password) as string;

        if (username?.trim() && password?.trim()) {
            login(username, password);
            history.push(Pages.dashboard())
        } else {
            store.dispatch({type: LoginActionTypes.LOGIN_ERROR, payload: 'Username and Password should be passed!'})
        }
    };

    return (
        <Form onSubmit={OnFormSubmit}>
            <Input placeholder={'Username'} name={formFields.username} imageSource={User}/>
            <Input placeholder={'Password'} type={'password'} name={formFields.password} imageSource={Lock}/>
            <Button disabled={loading}>{!loading ? 'Log In' : 'Logging...'}</Button>
        </Form>
    );
}

export default LoginForm;
