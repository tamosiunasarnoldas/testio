import { Dispatch } from "redux";
import axios from "axios";
import { ServersActions, ServersActionTypes } from "../../types/servers";
import { Api } from "../../types/Paths";

export const fetchServers = (token?: string) => {
    return async (dispatch: Dispatch<ServersActions>) => {
        try {
            dispatch({type: ServersActionTypes.GET_SERVERS})
            const response = await axios.get(Api.servers(), {
                headers: {
                    Authorization: token
                }
            });
            dispatch({type: ServersActionTypes.GET_SERVERS_SUCCESS, payload: response.data})
        } catch (error) {
            dispatch({type: ServersActionTypes.GET_SERVERS_ERROR, payload: error.message})
        }
    }
}
