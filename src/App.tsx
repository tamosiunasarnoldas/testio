import React from 'react';
import styled from "styled-components";
import { BrowserRouter, Redirect, Route } from 'react-router-dom';
import LoginView from "./Views/LoginView";
import DashboardView from "./Views/DashboardView";
import { Pages } from "./types/Paths";

const AppWrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  padding: 0;
  margin: 0;
`
const isAuthenticated = false;

const App = () => {
    return (
        <BrowserRouter>
            <AppWrapper>
                <Route path='/' exact render={() => {
                    return (
                        isAuthenticated
                            ? <Redirect to={Pages.dashboard()}/>
                            : <Redirect to={Pages.login()}/>
                    )
                }}/>
                <Route path={Pages.login()} exact>
                    <LoginView/>
                </Route>
                <Route path={Pages.dashboard()} exact>
                    <DashboardView/>
                </Route>
            </AppWrapper>
        </BrowserRouter>
    );
};

export default App;
