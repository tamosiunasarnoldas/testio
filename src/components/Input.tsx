import React from 'react';
import styled from "styled-components";
import { InputType } from "../types/InputType";

const InputField = styled.input`
  color: ${props => props.theme.defaultTextColor};
  padding: 1.5rem;
  padding-left: 4rem;
  background-color: white;
  border-radius: ${props => props.theme.defaultBorderRadius};
  width: 100%;
  box-sizing: border-box;
  border: none;
  outline: none;

  ::placeholder {
    color: ${props => props.theme.defaultPlaceHolderColor};
  }
`

const InputImage = styled.img`
  width: 20px;
  position: absolute;
  left: 25px;
  top: calc(50% - 12px);
  pointer-events: none;
`

const InputWrapper = styled.div`
  position: relative;
`

const Input = ({imageSource, placeholder, name, type}: InputType) => {
    return (
        <InputWrapper>
            <InputField name={name} type={type} placeholder={placeholder}/>
            <InputImage alt={''} src={imageSource}/>
        </InputWrapper>
    );
}

export default Input;
