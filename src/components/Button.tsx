import styled from "styled-components";

const Button = styled.button`
  width: 100%;
  padding: 1.5rem;
  background-color: ${props => props.theme.defaultButtonColor};
  color: ${props => props.theme.defaultButtonTextColor};
  border: none;
  border-radius: ${props => props.theme.defaultBorderRadius};
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: ${props => props.theme.defaultButtonColorHover};
  }

  &:active {
    background-color: ${props => props.theme.defaultButtonColorActive};
  }
  
  &:disabled {
    background-color: ${props => props.theme.defaultButtonColorDisabled};
    cursor: default;
  }
`
export default Button;
