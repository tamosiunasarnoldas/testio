import { combineReducers } from "redux";
import { loginReducer } from "./loginReducer";
import { serversReducer } from "./dahboardReducer";

export const rootReducer = combineReducers({
    login: loginReducer,
    servers: serversReducer
})

export type RootState = ReturnType<typeof rootReducer>
