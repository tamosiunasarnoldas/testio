import { Dispatch } from "redux";
import { LoginActions, LoginActionTypes } from "../../types/loginActions";
import axios from "axios";
import { Api } from "../../types/Paths";

export const login = (username: string, password: string) => {
    return async (dispatch: Dispatch<LoginActions>) => {
        try {
            dispatch({type: LoginActionTypes.LOGIN})
            const response = await axios.post(Api.login(), {
                username,
                password,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            dispatch({type: LoginActionTypes.LOGIN_SUCCESS, payload: response.data.token})
            localStorage.setItem('token', response.data.token)
        } catch (error) {
            dispatch({type: LoginActionTypes.LOGIN_ERROR, payload: error.message})
        }
    }
}
