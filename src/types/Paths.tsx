export const Pages = {
    login: () => '/login',
    dashboard: () => '/dashboard'
}

export const Api = {
    login: () => 'https://playground.tesonet.lt/v1/tokens',
    servers: () => 'https://playground.tesonet.lt/v1/servers'
}
