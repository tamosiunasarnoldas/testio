import * as LoginActionCreators from './login';
import * as ServersActionCreators from './servers';

export default {
    ...LoginActionCreators,
    ...ServersActionCreators
}
