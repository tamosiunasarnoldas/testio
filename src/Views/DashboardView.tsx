import React, { useEffect } from 'react';
import styled from "styled-components";
import dashboardLogo from "../assets/dashboardLogo.svg";
import door from "../assets/door.svg";
import { useHistory } from 'react-router-dom';
import { useTypedSelector } from "../hooks/useTypedSelector";
import { useActions } from "../hooks/useActions";
import { Server } from "../types/servers";
import { Pages } from "../types/Paths";

const Header = styled.nav`
  position: sticky;
  top: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 2rem;
  background-color: white;
`

const Logout = styled.span`
  display: flex;
  align-items: center;
  cursor: pointer;

  &:hover {
    color: #99cc33;
  }

  span {
    margin-left: 2rem;
  }
`

const ItemWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 2rem;
  color: #666666;
  background-color: white;
  border-top: 1px solid #E6E6E6;
`

const ServerListHeader = styled.div`
  position: sticky;
  display: flex;
  justify-content: space-between;
  top: 94px;
  background-color: #E6E6E6;
  color: #999999;
  padding: 1.5rem;
  
`

const ServerItem = ({name, distance}: Server) => {
    return (
        <ItemWrapper>
            <span>{name}</span>
            <span>{distance}</span>
        </ItemWrapper>
    )
}

const DashboardView: React.FC = () => {
    const {error, loading, servers} = useTypedSelector(state => state.servers);
    const token = localStorage.getItem('token');
    const history = useHistory();

    if (!token) {
        history.push(Pages.login());
    }

    const {fetchServers} = useActions();

    const logout = () => {
        localStorage.removeItem('token');
        history.push(Pages.login());
    }

    useEffect(() => {
        if (token) {
            fetchServers(token)
        }
    }, [token])

    if (loading) {
        return <h1>Loading ...</h1>;
    }

    if (error) {
        return <h1>{error}</h1>;
    }

    return (
        <>
            {
                <>
                    <Header>
                        <img alt={'Testio logo'} src={dashboardLogo}/>
                        <Logout onClick={() => {
                            logout()
                        }}>
                            <img alt={''} src={door}/>
                            <span>Logout</span>
                        </Logout>
                    </Header>
                    <ServerListHeader>
                        <span>SERVER</span>
                        <span>DISTANCE</span>
                    </ServerListHeader>
                    {
                        servers
                            ? servers.map((server, index) =>
                                <ServerItem
                                    key={index}
                                    name={server.name}
                                    distance={server.distance}
                                />)
                            : <h1>Servers not found!</h1>
                    }
                </>
            }
        </>
    );
};

export default DashboardView;
