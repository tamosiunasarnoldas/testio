import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { Provider } from "react-redux";
import { store } from "./store";

const Global = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
`

const theme = {
    defaultButtonColor: '#9FD533',
    defaultButtonColorHover: '#86b300',
    defaultButtonColorActive: '#9FD533',
    defaultButtonColorDisabled: '#9FD533',
    defaultButtonTextColor: 'white',
    defaultBorderRadius: '5px',
    defaultTextColor: '#999',
    defaultPlaceHolderColor: '#B3B3B3',
    errorColor: 'red',
    whiteColor: 'white'
}

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Provider store={store}>
            <Global/>
            <App/>
        </Provider>
    </ThemeProvider>,
    document.getElementById('root')
)
;
