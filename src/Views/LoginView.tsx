import React from 'react';
import styled from "styled-components";
import LoginForm from "../components/LoginForm";
import bgImage from "../assets/backgroundImage.png";
import loginLogo from "../assets/login_logo.svg";
import { useTypedSelector } from "../hooks/useTypedSelector";

const LoginViewContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100vh;
  width: 100%;
  background-image: url(${bgImage});
  background-color: black;
  background-repeat: no-repeat;
  background-size: cover;
`

const Login = styled.img`
 margin-bottom: 5rem; 
`

const LoginView: React.FC = () => {
    const {error} = useTypedSelector(state => state.login);

    const ErrorMessage = styled.div`
      position: absolute;
      top: 30px;
      padding: 2rem 6rem;
      background: ${props => props.theme.errorColor};
      color: ${props => props.theme.whiteColor};
      font-size: 2rem;
      border-radius: 5px;
    `

    return (
        <LoginViewContainer>
            {error && <ErrorMessage>{error}</ErrorMessage>}
            <Login src={loginLogo}/>
            <LoginForm/>
        </LoginViewContainer>
    )
}

export default LoginView;
