import { ServersActions, ServersActionTypes, ServersState } from "../../types/servers";

const initialState: ServersState = {
    loading: false,
    error: null,
    servers: []
}

export const serversReducer = (state = initialState, action: ServersActions): ServersState => {
    switch (action.type) {
        case ServersActionTypes.GET_SERVERS:
            return {
                ...initialState, loading: true
            }
        case ServersActionTypes.GET_SERVERS_SUCCESS:
            return {
                ...initialState, loading: false, servers: action.payload
            }
        case ServersActionTypes.GET_SERVERS_ERROR:
            return {
                ...initialState, loading: false, error: action.payload
            }
        default:
            return state
    }
}
